#! /usr/bin/python

import pygame

pygame.init()
screen = pygame.display.set_mode((400,300))
done = False

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    # left, top, width, height

    side = 60
    init = 30
    gap = 5
    
    pygame.draw.rect(screen, (255,0,0), pygame.Rect(init,init,side,side))
    pygame.draw.rect(screen, (0,255,0), pygame.Rect(init+side+gap,init,side,side))
    pygame.draw.rect(screen, (0,0,255), pygame.Rect(init+2*side+2*gap,init,side,side))

    pygame.display.flip()

