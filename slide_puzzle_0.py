import os
class Jigsaw:
# Useless comment


    def __init__(self):
        import random
        data = list(range(1,16))
        data.append(' ')
        random.shuffle(data)
        self.msg = ''
        self.next_move = []
        # This dictionary assigns values 1 to 15 and a space, shuffled into the matrix
        self.dic = {'a0':data[0], 'a1':data[1], 'a2':data[2], 'a3':data[3],
                    'b0':data[4], 'b1':data[5], 'b2':data[6], 'b3':data[7],
                    'c0':data[8], 'c1':data[9], 'c2':data[10], 'c3':data[11],
                    'd0':data[12], 'd1':data[13], 'd2':data[14], 'd3':data[15]} 
        # Table of valid directions for each key (coordinate a0 - d3)
        # The order is left, right, up, down
        self.valid = {'a0':[False, 'a1', False, 'b0'], 'a1':['a0', 'a2', False, 'b1'],
                      'a2':['a1', 'a3', False, 'b2'], 'a3':['a2', False, False, 'b3'], 
                      'b0':[False, 'b1', 'a0', 'c0'], 'b1':['b0', 'b2', 'a1', 'c1'], 
                      'b2':['b1', 'b3', 'a2', 'c2'], 'b3':['b2', False, 'a3', 'c3'], 
                      'c0':[False, 'c1', 'b0', 'd0'], 'c1':['c0', 'c2', 'b1', 'd1'], 
                      'c2':['c1', 'c3', 'b2', 'd2'], 'c3':['c2', False, 'b3', 'd3'], 
                      'd0':[False, 'd1', 'c0', False], 'd1':['d0', 'd2', 'c1', False], 
                      'd2':['d1', 'd3', 'c2', False], 'd3':['d2', False, 'c3', False]}
        self.won = {'a0':1, 'a1':2, 'a2':3, 'a3':4,
                    'b0':5, 'b1':6, 'b2':7, 'b3':8,
                    'c0':9, 'c1':10, 'c2':11, 'c3':12,
                    'd0':13, 'd1':14, 'd2':15, 'd3':' '} 
        # Convert the matrix dictionary items to lists, one for keys, one for values
        self.dic_keys = list(self.dic.keys())
        self.dic_vals = list(self.dic.values())
        # self.cursor returns the dictionary key (a0 - d3) of the location of the space (' ')
        self.cursor = self.dic_keys[self.dic_vals.index(' ')]

        
    def display(self):
        if 'bash' in os.environ['SHELL']:
            os.system('clear')
        else:
            os.system('cls')
        self.msg = ''
        self.next_move = []
        print('\n     0  1  2  3\n')
        print(' a  ', str(self.dic['a0']).ljust(2), str(self.dic['a1']).ljust(2), 
                      str(self.dic['a2']).ljust(2), str(self.dic['a3']).ljust(2))
        print(' b  ', str(self.dic['b0']).ljust(2), str(self.dic['b1']).ljust(2),
                      str(self.dic['b2']).ljust(2), str(self.dic['b3']).ljust(2))
        print(' c  ', str(self.dic['c0']).ljust(2), str(self.dic['c1']).ljust(2),
                      str(self.dic['c2']).ljust(2), str(self.dic['c3']).ljust(2))
        print(' d  ', str(self.dic['d0']).ljust(2), str(self.dic['d1']).ljust(2),
                      str(self.dic['d2']).ljust(2), str(self.dic['d3']).ljust(2))
        print("\n Space is currently at: ", self.cursor)
        self.msg += " \n Use the WASD keys to slide a number into the space. exit with 'q'\n"
        # The order in the zip is: 
        # d: left, a: right, s: up, w: down
        for i in zip(['d', 'a', 's', 'w'],self.valid[self.cursor]):
            if i[1] == False:
                pass
            else:
                self.next_move.append(str(i[0]))
        print(self.msg)
        

    
    def shift(self, dir):
        if dir in self.next_move:
            # Slide number to the left
            if dir == 'd':
                temp = self.dic[self.valid[self.cursor][0]]
                self.dic[self.cursor] = temp
                self.cursor = self.valid[self.cursor][0]
                self.dic[self.cursor] = ' '
            # Slide number to the right
            if dir == 'a':
                temp = self.dic[self.valid[self.cursor][1]]
                self.dic[self.cursor] = temp
                self.cursor = self.valid[self.cursor][1]
                self.dic[self.cursor] = ' '
            # Slide number above the space
            if dir == 's':
                temp = self.dic[self.valid[self.cursor][2]]
                self.dic[self.cursor] = temp
                self.cursor = self.valid[self.cursor][2]
                self.dic[self.cursor] = ' '
            # Slide number under the space
            if dir == 'w':
                temp = self.dic[self.valid[self.cursor][3]]
                self.dic[self.cursor] = temp
                self.cursor = self.valid[self.cursor][3]
                self.dic[self.cursor] = ' '
        
        
if __name__ == '__main__':
    user_input = ''
    j = Jigsaw()
    while user_input != 'q' :
        j.display()
        user_input = input(' > ')
        j.shift(user_input)

