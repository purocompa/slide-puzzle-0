pushing data from a different host
Testing insertion from original machine. In order to see changes on local reop run:

git clone

from within the repo's directory.

You might need to initialize an ssh agent and load your private key:

eval $(ssh-agent)
ssh-add ~/.ssh/<the key>


If retrieving your key from file, make sure to 
chmod 600 ~/.ssh/<the key>

